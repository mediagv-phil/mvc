<?php
include_once("system/Controller.php");

include_once("models/ModelMyAppli.php");
include_once("views/ViewMyAppli.php");

class CtrlMyAppli extends Controller
{
	function __construct($c)
	{
		parent::__construct($c);
		$this->setModel("ModelMyAppli");
		$this->setView("ViewMyAppli");
		$this->controle($c);
	}
	
	function accueil($c)
	{
		return array(
			'partial' => "Ma variable partielle dans ctrl my appli"
		);
	}
	
	function contact($c)
	{
		return array(
			
		);
	}
	
	function meteo()
	{
		$this->data = array(
			'content' => "meteo"
		);
		return $this->data;
	}
}

?>